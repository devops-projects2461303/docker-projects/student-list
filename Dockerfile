FROM python:3.8-buster
MAINTAINER Jean-Luc MPANDE
RUN mkdir data
COPY simple_api/student_age.py /
COPY simple_api/student_age.json /data/
COPY simple_api/requirements.txt /
COPY simple_api/wsgi.py /
RUN apt update -y && apt install python-dev python3-dev libsasl2-dev python-dev libldap2-dev libssl-dev -y
RUN pip install -r /requirements.txt
CMD gunicorn --bind 0.0.0.0:$PORT wsgi:app
